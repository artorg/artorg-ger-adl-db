#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Eleanore Young <eleanore.young@artorg.unibe.ch>
#
# Distributed under terms of the MIT license.

"""
Sign up or login as a Parse user and print the session key.

This may be used to enable embedded devices to communicate with Parse in an authenticated
fashion. By default, it creates restricted sessions.

If present, the script attempts to read the following variables from an .env file placed in the
script directory: SERVER_URL, APP_ID and MASTER_KEY. You can override these by using the command
line options. The .env file should have the same format as used by Heroku and NodeJS.
"""

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

import hashlib
import os

import click
import requests

from memorize import Memorize


@click.command()
@click.option("-s", "--server-url", help="The Parse server URL")
@click.option("-i", "--app-id", help="The Parse App ID")
@click.option("-k", "--master-key", help="The Parse App master key")
@click.option("-m", "--mode", type=click.Choice(["restricted", "full"]),
        default="restricted", help="Issue restricted or full session tokens")
@click.argument("username")
def main(server_url, app_id, master_key, mode, username):
    """
    Sign up or login as a Parse user and print the session key.

    This may be used to enable embedded devices to communicate with Parse in an authenticated
    fashion. By default, it creates restricted sessions.

    If present, the script attempts to read the following variables from an .env file placed in the
    script directory: SERVER_URL, APP_ID and MASTER_KEY. You can override these by using the command
    line options. The .env file should have the same format as used by Heroku and NodeJS.
    """
    # Determine the current location
    script_location = os.path.dirname(__file__)
    env_location = os.path.join(script_location, ".env")

    # Parse the .env file, if present
    if os.path.isfile(env_location):
        cfg = configparser.ConfigParser()
        with open(env_location) as env:
            cfg.read_string("\n".join(["[None]", env.read().replace("\"", "")]))
            server_url_cfg = cfg.get("None", "SERVER_URL", fallback="http://localhost/parse")
            app_id_cfg = cfg.get("None", "APP_ID", fallback="myAppId")
            master_key_cfg = cfg.get("None", "MASTER_KEY", fallback="myMasterKey")
    else:
        server_url_cfg = "http://localhost/parse"
        app_id_cfg = "myAppId"
        master_key_cfg = "myMasterKey"

    # Acquire the session token depending on the selected mode
    session_token = None
    if mode == "full":
        session_token = _login(
            server_url or server_url_cfg,
            app_id or app_id_cfg,
            master_key or master_key_cfg,
            username,
            _password(username)
        )
    elif mode == "restricted":
        session_token = _session(
            server_url or server_url_cfg,
            app_id or app_id_cfg,
            master_key or master_key_cfg,
            username,
            _password(username)
        )

    if session_token is not None:
        click.echo(session_token)


def _password(username):
    return hashlib.sha256(username.encode()).hexdigest()


@Memorize
def _signup(server_url, app_id, master_key, username, password):
    # Try to sign in as the user
    r = _signup_request(
        server_url,
        app_id,
        master_key,
        username,
        password
    )

    # Try to get the session token
    json_response =  r.json()
    if json_response.get("sessionToken") is not None:
        return json_response.get("sessionToken")
    else:
        raise ValueError("Failed to find the session token: {}".format(json_response))


@Memorize
def _login(server_url, app_id, master_key, username, password):
    # Try to sign up as that user first
    try:
        return _signup(
            server_url,
            app_id,
            master_key,
            username,
            password
        )

    except ValueError:
        # Try to log in as that user
        r = _login_request(
            server_url,
            app_id,
            username,
            password
        )

        # Try to get the unrestricted session token
        json_response = r.json()
        if json_response.get("sessionToken") is not None:
            return json_response.get("sessionToken")
        else:
            raise ValueError("Failed to find the session token: {}".format(json_response))


def _session(server_url, app_id, master_key, username, password):
    # Make sure that the user is logged in first
    session_token = _login(
        server_url,
        app_id,
        master_key,
        username,
        password
    )

    # Create the restricted session
    r = _session_request(
        server_url,
        app_id,
        session_token
    )

    # Try to get the restricted session token
    json_response = r.json()
    if json_response.get("sessionToken") is not None:
        return json_response.get("sessionToken")
    else:
        raise ValueError("Failed to find the restricted session token: {}".format(json_response))


def _signup_request(server_url, app_id, master_key, username, password):
    parse_url = "/".join([server_url, "classes/_User"])
    headers = {
        "X-Parse-Application-Id": app_id,
        "X-Parse-Master-Key": master_key,
        "X-Parse-Revocable-Session": 1
    }
    data = {
        "username": username,
        "password": password
    }
    return requests.post(parse_url, headers=headers, json=data)


def _login_request(server_url, app_id, username, password):
    parse_url = "/".join([server_url, "login"])
    headers = {
        "X-Parse-Application-Id": app_id,
        "X-Parse-Revocable-Session": 1,
    }
    data = {
        "username": username,
        "password": password
    }
    return requests.get(parse_url, headers=headers, data=data)


def _session_request(server_url, app_id, session_token):
    parse_url = "/".join([server_url, "classes/_Session"])
    headers = {
        "X-Parse-Application-Id": app_id,
        "X-Parse-Session-Token": session_token,
    }
    return requests.post(parse_url, headers=headers)


if __name__ == "__main__":
    main()

