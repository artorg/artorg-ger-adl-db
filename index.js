/*
 * index.js
 * Copyright (C) 2016 Eleanore Young <eleanore.young@artorg.unibe.ch>
 *
 * Distributed under terms of the MIT license.
 */
'use strict';
if (process.env.NEW_RELIC_LICENSE_KEY) {
    require ('newrelic');
}

var express = require ('express');
var path = require ('path');
var parse_server = require ('parse-server');
var ParseDashboard = require ('parse-dashboard');
var ParseServer = parse_server.ParseServer;

// Some settings may be set using environment variables.
var serverPort = process.env.PORT || 5000;
var mountPath = process.env.PARSE_MOUNT || '/parse';
var dashboardSubpath = process.env.DASHBOARD_MOUNT || '/dashboard';
var appName = process.env.APP_NAME || 'myAppName';
var appId = process.env.APP_ID || 'myAppId';
var masterKey = process.env.MASTER_KEY || 'myMasterKey';
var serverURL = process.env.SERVER_URL || 'http://localhost:' + serverPort + mountPath;
var databaseUri = process.env.MONGODB_URI || process.env.MONGOLAB_URI || 'mongodb://localhost:27017/dev';
var cloudCode = process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js';
var productionEnv = process.env.PRODUCTION || false;
var dashboardUser = process.env.DASHBOARD_USER || 'admin';
var dashboardPass = process.env.DASHBOARD_PASS || 'password';
var allowInsecure = process.env.PARSE_DASHBOARD_ALLOW_INSECURE_HTTP || false;

var api = new ParseServer ({
    databaseURI: databaseUri,
    cloud: cloudCode,
    appId: appId,
    masterKey: masterKey,
    serverURL: serverURL
});

var dashboard = new ParseDashboard ({
    "apps": [
    {
        "serverURL": serverURL,
        "appId": appId,
        "masterKey": masterKey,
        "appName": appName,
        "production": productionEnv,
        "allowInsecureHTTP": allowInsecure
    }
    ],
    "users": [
    {
        "user": dashboardUser,
        "pass": dashboardPass
    }
    ]
});

// Create the Express app
var app = express ();

// Make the Express app work behind a proxy
app.enable ('trust proxy');

// Make the Parse Server available at /parse
app.use (mountPath, api);

// Make the Parse Dashboard available at /dashboard
app.use (dashboardSubpath, dashboard);

// Serve static assets from the /public folder
app.use (express.static (path.join (__dirname, 'public'), {dotfiles: 'allow'}));

// Send response on root get
app.get('/', function(req, res) {
    res.status(200).send('Connect to parse Server with subpath /parse or to the dashboard with subpath /dashboard.');
});

// Run the server
app.listen(serverPort, function() {
    console.log('Parse server and dashboard are running on port ', serverPort);
});
