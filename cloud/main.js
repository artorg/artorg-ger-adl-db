/*
 * main.js
 * Copyright (C) 2016 Eleanore Young <eleanore.young@artorg.unibe.ch>
 *
 * Distributed under terms of the MIT license.
 */
'use strict';

Parse.Cloud.define ('hello', function (req, res) {
    res.success ('Hi');
});
