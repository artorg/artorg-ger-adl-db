ARTORG-GER-ADL-DB Database
==========================
This is an implementation of the Parse server and dashboard in one
Heroku-capable instance. There's not much to say here other than that you will
need to create a Heroku app from this repository, set the configuration
variables used in ``index.js`` on the Heroku configuration page and deploy. This
particular Heroku app relies on a MongoDB and the NewRelic telemetry addons. For
example:

.. code-block:: shell

    $ heroku login
    $ heroku create database
    $ heroku addons:create --app database newrelic:wayne
    $ heroku addons:create --app database mongolab:sandbox
    $ heroku push

The environment variables used by the Parse server and dashboard are (including
the default values):

.. code-block::

    PORT = 5000
    PARSE_MOUNT = "/parse"
    DASHBOARD_MOUNT = "/dashboard"
    APP_NAME = "myAppName"
    APP_ID = "myAppId"
    MASTER_KEY = "myMasterKey"
    SERVER_URL = "http://localhost:$PORT/$PARSE_MOUNT"
    MONGODB_URI = "mongodb://localhost:27017/dev"
    CLOUD_CODE_MAIN = "$PWD/cloud/main.js"
    PRODUCTION = 0
    DASHBOARD_USER = "admin"
    DASHBOARD_PASS = "password"
    PARSE_DASHBOARD_ALLOW_INSECURE_HTTP = 0
    NEW_RELIC_LICENSE_KEY = no default value

Creating Parse Users
--------------------
The Python script ``parse_session.py`` can be used to create Parse users and
sessions for use in the other ARTORG-GER-ADL-... projects. From the usage
message:

.. code-block::

    Usage: parse_session.py [OPTIONS] USERNAME

        Sign up or login as a Parse user and print the session key.

        This may be used to enable embedded devices to communicate with Parse in an authenticated
        fashion. By default, it creates restricted sessions.

        If present, the script attempts to read the following variables from an .env file placed in the
        script directory: SERVER_URL, APP_ID and MASTER_KEY. You can override these by using the command
        line options. The .env file should have the same format as used by Heroku and NodeJS.

    Options:
        -s, --server-url TEXT           The Parse server URL
        -i, --app-id TEXT               The Parse App ID
        -k, --master-key TEXT           The Parse App mater key
        -m, --mode [restricted|full]    Issue restricted or full session tokens
        --help                          Show this message and exit.
